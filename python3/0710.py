card_infors = []
def print_menu():  #定义一个空字典
    print("="*50)
    print("名片管理系统")
    print("1.添加一个新的名片")
    print("2.删除一个名片")
    print("3.修改一个名片")
    print("4.查询一个名片")
    print("5.显示所有名片")
    print("6.退出")
    print("="*50)
def add_new_card_infor(): #定义一个函数（菜单）
    new_name = input("new name :")
    new_weixin = input("new wx:")

    new_infor = {} #定义空字典
    new_infor['name'] = new_name #读取input键入值，new_name存储为name
    new_infor['weixin'] = new_weixin #读取input键入值，new_weixin存储为weixin

    global card_infors #对应全局变量card_infors
    card_infors.append(new_infor) #在全局变量card_infors列表中新增函数内的new_infor

def del_card_infor():
    del_name = input("please input del name:")
    del_weixin = input("please input del weixin:")
    del_infor = {}
    del_infor['name'] = del_name
    del_infor['weixin'] = del_weixin
    global card_infors
    card_infors.remove(del_infor)

def modify_card_info():
    '''
    modify card info
    '''
    global card_infors
    # position is list's index
    position = card_infors.index({'name':input("please input modify old name:"),'weixin':input("please input modify old weixin:")})
    card_infors[position] = {'name':input("please input new name:"),'weixin':input("please input new name:")}
    print('modify finish')


def find_card_infor(): #定义一个函数（查找）
    global card_infors #对应全局变量card_infors
    find_name = input("find name:") 
    find_flag = 0 #find_flag定义为0,用于下面做判断
    for temp in card_infors: #temp遍历card_infors循环
        if find_name == temp["name"]: #判断：当键入名称等于temp的name值（相当于for去查找card_infors值）打印如下
            print("%s\t%s\t"%(temp['name'],temp['weixin'])) #打印temp(card_infors)及temp(card_infors)
            find_flag=1 #判断这里原变量值为0，这里等1，所以结束if循环
            break #结束if循环
     #   else:
      #      print("no..")
      #      break

    if find_flag == 0: #表示没有找到，匹配定义的变量0，所以打印no
        print("no..")

def show_all_infor(): #定义一个函数（查看所有）

    global card_infors

    print("name\tweixin") #首先打印name及weixin，使用\t制表符，方便查看
    for temp in card_infors: #循环，temp至card_infors
        print("%s\t%s\t"%(temp['name'],temp['weixin'])) #打印card_infors里所有信息

def main(): #定义全局函数
    print_menu() #调用函数

    while True: #无限循环
        num = int(input("please num:")) #打印请输入num
        if num == 1: #判断用户输入
            add_new_card_infor() #调用新增函数
        elif num == 2 :
            del_card_infor() #删除函数
        elif num == 3 :
            modify_card_info()
        elif num == 4 :
            find_card_infor()
        elif num == 5:
            show_all_infor()
        elif num == 6:
            break
        else:
            print("please again input")

        print("") #打印换行

main()
